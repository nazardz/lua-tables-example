a ={}  -- empty table

table.insert(a, 16) -- add first element

a['key'] = 13 -- add element with key

print(a.key) 

a.key = 14 -- change element in key

print(a['key'])

print("size = ", #a)

-- a['key'] == a.key 
-- key == index
-- 13
-- 14
-- size = 	1

a[2] = 17 -- add 'second' element

print("size = ", #a)

-- table is (dict and list)
-- size =   2

number = 9

a[number] = 15 -- add ninth element

print(a[9])

print("size = ", #a)

-- dict keys can be number because table is also list
-- 15
-- size = 	2

for i = 1,#a do
    print(a[i]) 
end

-- but list can't reach numeric keys
-- 16
-- 17

i = 1
while i < number do
    table.insert(a, i)
    i = i+1
end

for i = 1,#a do
    print(a[i])
end

print("size = ", #a)

-- filled table to reach numeric key 
-- 16
-- 17
-- 1
-- 2
-- 3
-- 4
-- 5
-- 6
-- 15
-- 7
-- 8
-- size = 	11
